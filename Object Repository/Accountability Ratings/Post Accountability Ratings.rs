<?xml version="1.0" encoding="UTF-8"?>
<WebServiceRequestEntity>
   <description></description>
   <name>Post Accountability Ratings</name>
   <tag></tag>
   <elementGuidId>37bab180-efb6-4de7-bb27-84081209cdb8</elementGuidId>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <followRedirects>true</followRedirects>
   <httpBody></httpBody>
   <httpBodyContent>{
  &quot;text&quot;: &quot;{\n  \&quot;id\&quot;: \&quot;\&quot;,\n  \&quot;ratingTitle\&quot;: \&quot;Accountability Rating\&quot;,\n  \&quot;educationOrganizationReference\&quot;: {\n    \&quot;educationOrganizationId\&quot;: 255901001,\n    \&quot;link\&quot;: {\n      \&quot;rel\&quot;: \&quot;School\&quot;,\n      \&quot;href\&quot;: \&quot;/ed-fi/schools/76ae18e7c3d14084b37699c9467d3696\&quot;\n    }\n  },\n  \&quot;schoolYearTypeReference\&quot;: {\n    \&quot;schoolYear\&quot;: ${schoolYear},\n    \&quot;link\&quot;: {\n      \&quot;rel\&quot;: \&quot;SchoolYearType\&quot;,\n      \&quot;href\&quot;: \&quot;/ed-fi/schoolYearTypes/a3f58649593447cd98dfa069e5623983\&quot;\n    }\n  },\n  \&quot;rating\&quot;: \&quot;Academically Acceptable\&quot;,\n  \&quot;ratingDate\&quot;: \&quot;2019-07-18\&quot;,\n  \&quot;ratingOrganization\&quot;: \&quot;SEA\&quot;,\n  \&quot;ratingProgram\&quot;: \&quot;AEIS\&quot;,\n  \&quot;_etag\&quot;: \&quot;5248573597447357904\&quot;\n}&quot;,
  &quot;contentType&quot;: &quot;application/json&quot;,
  &quot;charset&quot;: &quot;UTF-8&quot;
}</httpBodyContent>
   <httpBodyType>text</httpBodyType>
   <httpHeaderProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>Content-Type</name>
      <type>Main</type>
      <value>application/json</value>
   </httpHeaderProperties>
   <httpHeaderProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>Authorization</name>
      <type>Main</type>
      <value>Bearer 19000fdb5a8e4003a9666db013aad6ee</value>
   </httpHeaderProperties>
   <migratedVersion>5.4.1</migratedVersion>
   <restRequestMethod>POST</restRequestMethod>
   <restUrl>https://api.ed-fi.org:443/v3.1.1/api/data/v3/ed-fi/accountabilityRatings</restUrl>
   <serviceType>RESTful</serviceType>
   <soapBody></soapBody>
   <soapHeader></soapHeader>
   <soapRequestMethod></soapRequestMethod>
   <soapServiceFunction></soapServiceFunction>
   <variables>
      <defaultValue>'2018'</defaultValue>
      <description></description>
      <id>09ec04a6-8e58-434d-b686-5a4675acd547</id>
      <masked>false</masked>
      <name>schoolYear</name>
   </variables>
   <verificationScript>import static org.assertj.core.api.Assertions.*

import com.kms.katalon.core.testobject.RequestObject
import com.kms.katalon.core.testobject.ResponseObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webservice.verification.WSResponseManager

import groovy.json.JsonSlurper
import internal.GlobalVariable as GlobalVariable

RequestObject request = WSResponseManager.getInstance().getCurrentRequest()

ResponseObject response = WSResponseManager.getInstance().getCurrentResponse()

WS.verifyResponseStatusCode(response, 201)</verificationScript>
   <wsdlAddress></wsdlAddress>
</WebServiceRequestEntity>
