<?xml version="1.0" encoding="UTF-8"?>
<WebServiceRequestEntity>
   <description>Get all the accountability ratings.</description>
   <name>Get All Accountability Ratings</name>
   <tag></tag>
   <elementGuidId>e4e6ec17-cc3f-4a8b-87c6-b12d2f4f7aec</elementGuidId>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <followRedirects>false</followRedirects>
   <httpBody></httpBody>
   <httpBodyContent></httpBodyContent>
   <httpBodyType></httpBodyType>
   <httpHeaderProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>Authorization</name>
      <type>Main</type>
      <value>Bearer 76f1e055f7674193829b3f3bedb8eb5f</value>
   </httpHeaderProperties>
   <migratedVersion>5.4.1</migratedVersion>
   <restRequestMethod>GET</restRequestMethod>
   <restUrl>https://jsonblob.com/b6a3172b-ad49-11e9-9f45-dd7abd340cd9</restUrl>
   <serviceType>RESTful</serviceType>
   <soapBody></soapBody>
   <soapHeader></soapHeader>
   <soapRequestMethod></soapRequestMethod>
   <soapServiceFunction></soapServiceFunction>
   <verificationScript>import static org.assertj.core.api.Assertions.*

import com.kms.katalon.core.testobject.RequestObject
import com.kms.katalon.core.testobject.ResponseObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webservice.verification.WSResponseManager

import groovy.json.JsonSlurper
import internal.GlobalVariable as GlobalVariable

RequestObject request = WSResponseManager.getInstance().getCurrentRequest()

ResponseObject response = WSResponseManager.getInstance().getCurrentResponse()

WS.verifyResponseStatusCode(response, 200)

WS.verifyElementPropertyValue(response, &quot;[0].id&quot;, '93393446e9424522a4f28dfaed297c1d')

WS.verifyElementPropertyValue(response, '[0].schoolYearTypeReference.link.rel', 'SchoolYearType')

</verificationScript>
   <wsdlAddress></wsdlAddress>
</WebServiceRequestEntity>
