package accountabilityRatingKeywords

import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

import internal.GlobalVariable

public class WriteExcel {

	@Keyword
	public void writeAccountabilityRatingsToExcel(ArrayList parsedJson) {
		Xls_Reader reader=new Xls_Reader("D:\\EDFI Automation\\Test Data Sheets\\Accountability Ratings Sheet.xlsx");
		//reader.addSheet("Accountability Ratings")
		reader.addColumn("Sheet1", "Accountability Ratings Id")
		reader.addColumn("Sheet1", "Ratings")
		reader.addColumn("Sheet1", "Ratings Title")
		reader.addColumn("Sheet1", "School Year")

		for(int i=2;i<parsedJson.size+2;i++)
		{
			String accountabilityRatingId=parsedJson.get(i-2).id
			reader.setCellData("Sheet1", "Accountability Ratings Id", i, accountabilityRatingId)

			String ratings=parsedJson.get(i-2).rating
			reader.setCellData("Sheet1", "Ratings", i, ratings)

			String ratingTitle=parsedJson.get(i-2).ratingTitle
			reader.setCellData("Sheet1", "Ratings Title", i, ratingTitle)

			String schoolYear=parsedJson.get(i-2).schoolYearTypeReference.schoolYear
			reader.setCellData("Sheet1", "School Year", i, schoolYear)
		}

	}

	@Keyword
	public void writeStudentAssessmentsToExcel(ArrayList parsedJson) {
		Xls_Reader reader=new Xls_Reader("D:\\EDFI Automation\\Test Data Sheets\\Accountability Ratings Sheet.xlsx");
		reader.addSheet("Student Assessments")
		reader.addColumn("Student Assessments", "Student Assessment Id")
		reader.addColumn("Student Assessments", "Assessment Identifier")
		reader.addColumn("Student Assessments", "Student Unique Id")

		for(int i=2;i<parsedJson.size+2;i++)
		{
			String studentAssessmentId=parsedJson.get(i-2).id
			reader.setCellData("Student Assessments", "Student Assessment Id", i, studentAssessmentId)

			String assessmentIdentifier=parsedJson.get(i-2).assessmentReference.assessmentIdentifier
			reader.setCellData("Student Assessments", "Assessment Identifier", i, assessmentIdentifier)

			String studentUniqueId=parsedJson.get(i-2).studentReference.studentUniqueId
			reader.setCellData("Student Assessments", "Student Unique Id", i, studentUniqueId)
		}

	}

}
