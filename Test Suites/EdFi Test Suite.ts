<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>EdFi Test Suite</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>c6791fd7-f1aa-469a-a0b8-8e84bf4e3a6d</testSuiteGuid>
   <testCaseLink>
      <guid>4895d511-3632-4c19-8858-05cced83a328</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Accountability Ratings/Get Accountability Ratings By Id</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>9b90931e-74b4-4594-ab22-f2aa70743825</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Accountability Ratings/Get All Accountability Ratings</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>fd10b4cd-b960-446d-ba58-f6773fc0d0e7</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Accountability Ratings/Post Accountability Ratings</testCaseId>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>7c47b885-0421-4f17-9573-ae1a336d89d7</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>8a384f36-34dc-492c-9949-3c6caae1ee2b</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Accountability Ratings/Put Accountability Ratings By Id</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>6ad872b7-d16c-4557-87d2-0cb8019b955a</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Location Education Agencies/Get All Local Education Agencies</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>2113814a-f7f4-46e6-a7cf-8398be939836</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Student Assessments/Get All Student Assessments</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
